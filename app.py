from flask import Flask, render_template, request, redirect, url_for
import serial
import time
import os
import threading 
import time
import math
from math import sin, cos, sqrt, atan2, radians
from time import gmtime, strftime

running = False
app = Flask(__name__)

SERIAL_PORT = "/dev/ttyACM0"
gps = serial.Serial(SERIAL_PORT, baudrate = 9600, timeout = 0.5)

background = None
debug = False
i = 0
lonOld = None
latOld = None
total_distance = 0.0
coordResult = None

fileRecordingName = "trackingfile.txt"

def distanceCalc(lat, lon):
    global debug
    # approximate radius of earth in km   
    R = 6373.0
    lat1 = radians(abs(lat[0]))
    lon1 = radians(abs(lon[0]))
    lat2 = radians(abs(lat[1]))
    lon2 = radians(abs(lon[1]))
    if debug == True:
        print ("radians: ",lat1, lat2, lon1, lon2)
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2* atan2(sqrt(a), sqrt(1 - a))

    distance = R*c

    return distance

# Inherting the base class 'Thread' 
class AsyncWrite(threading.Thread):

    def __init__(self, text, out):

        # calling superclass init 
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        self.text = text
        self.out = out

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        global running
        global debug
        f = open(self.out, "a")
        while running == True:
            try:
                coord = getPositionData(gps)
                time.sleep(1.0)
            except (KeyboardInterrupt):
                gps.close()
          
            if coord[1] != "":
                f.write(coord[1])
                f.write("\n")
        f.close()

        # waiting for 2 seconds after writing 
        # the file 
        time.sleep(2)
        if debug == True:
            print("Finished background file write to",
                                             self.out)

@app.route('/', methods=['GET'])
def dreamBoard():
    global coordResult
    global running
    global debug
    global fileRecordingName
    try:
        if debug == True:
            print ("Application started!")
        if running == False:
            coordResult = getPositionData(gps)
            time.sleep(1.0)
        if coordResult == None or coordResult[0] == None:
           coordResult = [{"GPS:":"nessuna coordinata"}]
        #if debug == True:
        #    print("coord: " + str(coordResult[0]))
    except (KeyboardInterrupt):
       gps.close()
       if debug == True:
           print ("Applications closed!")
    if debug == True:
        print("running:  " + str(running))
    return render_template('tracker.html', coords = coordResult[0], trackerActive = "OFF!" if running == False else "ON!", fileRecordingName = fileRecordingName) 


@app.route('/tracker', methods=['POST'])
def trackerHandler():
    
    if request.form['tracker'] == 'stopTracker':
       stopRecording()
    elif request.form['tracker'] == 'startTracker':
       file_to_use = request.form.get("file_to_use")
       startRecording(file_to_use)
    
    return redirect(url_for('dreamBoard'))

def stopRecording():
    global background
    global running
    global debug
    global i
    global latOld
    global lonOld
    global total_distance
    running = False
    if background != None:
        background.stop()
        background.join()
        i = 0
        latOld = None
        lonOld = None
        total_distance = 0.0
        if debug == True:
            print("stopping tracker")

def startRecording(fileRecName):
    global background
    global running
    global fileRecordingName
    fileRecordingName = fileRecName
    if running == False:
        running = True
        background = AsyncWrite("message", fileRecordingName if fileRecordingName != None else "trackTest.txt") 
        background.start() 

def getPositionData(gps):
    global debug
    found = [False, False]
    global coordResult
    count = 0
    global i
    global total_distance
    global latOld
    global lonOld
    coordToSave = ""
    coord = None
    if debug == True:
        print("GET POSITION DATA")
    while (found[0] == False and found[1] == False) or count < 10:
        data = gps.readline()
        data1 = data.decode('latin-1')
        if debug == True:
            print(str(data1))
        message = data1[0:6]
        if debug == True:
            print(str(message))

        if (message == "$GPRMC" or message == "$GPGGA"):
            count = count + 1
            # GPRMC = Recommended minimum specific GPS/Transit data
            parts = data1.split(",")
            if len(parts) > 0 and message == "$GPRMC" and found[0] == False:
                if parts[2] == 'V':
                    # V = Warning, most likely, there are no satellites in view...
                    coord = {"GPS:": "receiver warning"}
                    if debug == True:
                        print ("GPRMC V")
               
                else:       
                    found[0] = True
                    lat = parts[3]
                    lon = parts[5]
                    lat_direction = data[4]
                    lon_direction = data[6]
                    lat = round(math.floor(float(lat) / 100) + (float(lat) % 100) / 60, 6)
                    if lat_direction == 'S':
                        lat = lat * -1

                    lon = round(math.floor(float(lon) / 100) + (float(lon) % 100) / 60, 6)
                    if lon_direction == 'W':
                        lon = lon * -1

                    if i == 0:
                        lonOld = lon
                        latOld = lat
                        i = 1
                    total_distance = total_distance + distanceCalc([lonOld, lon],[latOld, lat])
                    lonOld = lon
                    latOld = lat
                    #1 knot is exactly equal to 1.852 km/h
                    speed = float(parts[7])/1.852
                    timest = parts[1]
                    print("timestamp " + timest)
                    #tempo = datetime.fromtimestamp(float(timest))#time.ctime(float(timest))
                    tempo = strftime("%Y-%m-%d %H:%M:%S", gmtime())
                    coordToSave = str(lat) +","+ str(lon)
                    if coord == None:
                        coord = {"GPS:": "lon= " + str(lon) + ", lat= " + \
                             str(lat), "speed=": str(speed), "time=": str(tempo), "Km=": str(total_distance)}
                    else:
                        coord["GPS:"] = "lon= " + str(lon) + ", lat= "+ str(lat)
                        coord["speed="] = str(speed)
                        coord["time="] = str(tempo)
                        coord["Km="] = str(total_distance)                  
                    if debug == True:
                        print ("GPRMC OK")
            elif message == "$GPGGA" and len(parts) > 0 and found[1] == False:
                found[1] = True
                altitude = parts[9]
                numberOfSatellites = parts[7]
                if coord != None:                
                    coord["altitude:"] = altitude
                    coord["number of satellites:"] = numberOfSatellites 
                else:
                    coord = {"altitude:": altitude, "number of satellites:": numberOfSatellites}  
                if debug == True:
                    print("GPGGA OK")    

    coordResult = [coord, coordToSave]
            
    return coordResult    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port = 5000)

